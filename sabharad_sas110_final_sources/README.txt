Project name: Parallel Computing on Raspberry Pi Cluster
Team Members: Sachin Bharadwaj Sundramurthy, Sahil Sharma
Email Ids: sabharad@eng.ucsd.edu , sas110@eng.ucsd.edu
PID: A53221448, A53218656

Target: Raspberry Pi Model 3B

Build Environment:
Build natively on each Raspberry Pi
- For sequential programs, the command format is:
        g++ governor/governor_part1.c omp/prime/prime_seq.cpp -o prime_omp `pkg-config --cflags --libs opencv` -fopenmp
- For OpenMP programs, the command format is:
        g++ governor/governor_part1.c omp/prime/prime_openmp.c -o prime_omp `pkg-config --cflags --libs opencv` -fopenmp
- For MPI programs, the command format is:
        mpicc mpi/sobel/sobel_mpi.c -o mpi_sobel -lm
- For Hybrid OpenMP MPI programs, the command format is:
        mpicc mpi/sobel/sobel_hybrid.c -o hybrid_sobel -lm -fopenmp

Sequential and OpenMP codes can be run normally using "sudo ./<binary>". Input image file is expected to be
in the same directory as binary. Output will be in the same directory too.

MPI needs a machine file (one used in the project is located in mpi directory).
Executing command for MPI code is given in master

mpiexec -f <path to machine file in master> -n <number of nodes in cluster, 4 for us> <absolute path to compiled binary done via mpicc>

example execution mpiexec -f ../../machinefile -n 4 /home/pi/es/es_project/hybrid_sobel

Notes: The mpi binary needs to be present in each node, at the same location (/home/pi/es/es_project/hybrid_sobel in our case).
We used git repositories in each node's home directory and built the binary
individually on each RPI (thus achieving the same location and name).

Some codebases require .jpg image and some require .dat. We have written 2 files to do the conversion.
In formatConversion/ one can use dat_to_output.cpp to display .dat outputs to screen.
In formatConversion/ one can use jpeg_to_dat.cpp to create inputs for the code bases.
Beware, all files use hard coded image names and paths.
