/* This file contains the MPI implementation of RGB to grayscale conversion */

#include<omp.h>
#include<unistd.h>
#include<math.h>
#include <stdio.h>
#include "mpi.h"
#include <stdlib.h>

int main(int argc, char *argv[])
{
    unsigned char *arr1=NULL,*arr2=NULL,*arr3=NULL;
    int rank,numtasks;
    int rows, cols;
    int count=0;
    int tcount=0;
    unsigned char* red=NULL;
    unsigned char* blue=NULL;
    unsigned char* green=NULL;
    unsigned char* grey=NULL;
    unsigned char* data=NULL;
    unsigned char* out;

    // Initialize MPI world
    MPI_Init(&argc,&argv);
    // Get communication rank
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    // Get world size
    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);

    // Read image only in master
    if(rank==0)
    {
        // Read image
        FILE *f = fopen("image1.dat", "rb");
        fread(&rows, sizeof(int), 1, f);
        printf("\nRows: %d",rows);
        fread(&cols, sizeof(int), 1, f);
        printf("\nCols: %d",cols);
        arr1 = (unsigned char *) malloc(rows*cols*sizeof(unsigned char));
        arr2 = (unsigned char *) malloc(rows*cols*sizeof(unsigned char));
        arr3 = (unsigned char *) malloc(rows*cols*sizeof(unsigned char));
        out = (unsigned char *) malloc(rows*cols*sizeof(unsigned char));
        int  r1 = fread( arr1, sizeof(unsigned char), rows*cols, f);
        int r2 = fread( arr2, sizeof(unsigned char), rows*cols, f);
        int r3 = fread(arr3, sizeof(unsigned char), rows*cols, f);
        fflush(stdout);
        fclose(f);
        if((rows*cols)%numtasks!=0)
        {
            printf("\nError not a multiple");
            return 1;
        }
        count = (rows*cols)/numtasks;
        tcount = rows*cols;
    }

    // Broadcast the size of images
    MPI_Bcast(&count,1,MPI_INT,0,MPI_COMM_WORLD);
    MPI_Bcast(&tcount,1,MPI_INT,0,MPI_COMM_WORLD);
    printf("\n Count per process : %d ",count);
    // Allocate memory for images locally
    blue=(unsigned char *) malloc (count*sizeof(unsigned char));
    red =(unsigned char *) malloc (count*sizeof(unsigned char));
    green =(unsigned char *) malloc (count*sizeof(unsigned char));
    grey=(unsigned char *) malloc (count*sizeof(unsigned char));

    // Measure starting time
    double start_time,end_time;
    start_time = MPI_Wtime();

    // Scatter parts of RGB images to different nodes
    MPI_Scatter(arr1,count,MPI_UNSIGNED_CHAR,blue,count,MPI_UNSIGNED_CHAR,0,MPI_COMM_WORLD);
    MPI_Scatter(arr2,count,MPI_UNSIGNED_CHAR,green,count,MPI_UNSIGNED_CHAR,0,MPI_COMM_WORLD);
    MPI_Scatter(arr3,count,MPI_UNSIGNED_CHAR,red,count,MPI_UNSIGNED_CHAR,0,MPI_COMM_WORLD);

    // Convert image from RGB to Grayscale
    int i;
    for(i=0; i<count; i++)
        grey[i] = .299f * red[i]+0.587f*green[i] + 0.114f*green[i];

    // Gather parts of Grayscale images from different nodes
    MPI_Gather(grey,count,MPI_UNSIGNED_CHAR,out,count,MPI_UNSIGNED_CHAR,0,MPI_COMM_WORLD);

    // Measure end time
    end_time=MPI_Wtime();

    // Write image only in master
    if(rank==0)
    {
        // Write Images to files
        printf("Done coverting grey scale. Time %lf", end_time - start_time);
        FILE *f = fopen("output.dat", "wb");
        fwrite(&rows, sizeof(int), 1, f);
        fwrite(&cols, sizeof(int), 1, f);
        fwrite ((unsigned char *)out, sizeof(unsigned char), rows*cols,f);
        printf("%d %d\n", rows, cols);
        fflush(f);
        fclose(f);
        free(data);
        free(arr1);
        free(arr2);
        free(arr3);
    }
    // End MPI process
    MPI_Finalize();
    return 0;
}