/* This file contains the MPI implementation of prime number generation */

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define LIMIT    (1024*1024*8)     /* Total numbers to be scanned */

/* Function to check if a number is prime or not */
int isprime(int n)
{
    int i,squareroot;
    if (n>10)
    {
        // Get square rooot
        squareroot = (int) sqrt(n);
        // Check if the number is prime or not
        for (i=3; i<=squareroot; i=i+2)
            // Return 0 if the number is divisible by any number
            if ((n%i)==0)
                return 0;
        return 1;
    }
    else
        return 0;
}

int main (int argc, char *argv[])
{
    int   ntasks,               /* total number of tasks in partitiion */
          rank,                 /* task identifier */
          n,                    /* loop variable */
          prime_count,          /* prime counter */
          pcsum,                /* number of primes found by all tasks */
          recent_prime,         /* most recent prime found */
          maxprime,             /* largest prime found */
          mystart,              /* where to start calculating */
          stride;               /* calculate every nth number */

    // Set governor for changing frequency
    char szOldGovernor[32];
    set_governor("userspace", szOldGovernor);
    set_by_max_freq();

    double start_time,end_time;

    // Initialize MPI world
    MPI_Init(&argc,&argv);
    // Get communication rank
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    // Get world size
    MPI_Comm_size(MPI_COMM_WORLD,&ntasks);
    if (((ntasks%2) !=0) || ((LIMIT%ntasks) !=0))
    {
        printf("evenly divisible into %d.  Try 4 or 8.\n",LIMIT);
        // End MPI process
        MPI_Finalize();
        exit(0);
    }
    // Initialize start time
    start_time = MPI_Wtime();
    // Find my starting point - must be odd number
    mystart = (rank*2)+1;
    // Determine stride
    stride = ntasks*2;
    prime_count=0;
    recent_prime = 0;

    // Master
    if (rank == 0)
    {
        printf("Using %d tasks to scan %d numbers\n",ntasks,LIMIT);
        prime_count = 4;
        for (n=mystart; n<=LIMIT; n=n+stride)
        {
            if (isprime(n))
            {
                prime_count++;
                recent_prime = n;
            }
        }
        // Share the prime number count with the world
        MPI_Reduce(&prime_count,&pcsum,1,MPI_INT,MPI_SUM,0,MPI_COMM_WORLD);
        // Share the max prime
        MPI_Reduce(&recent_prime,&maxprime,1,MPI_INT,MPI_MAX,0,MPI_COMM_WORLD);
        // Measure end time
        end_time=MPI_Wtime();
        printf("Done. Largest prime is %d Total primes %d\n",maxprime,pcsum);
        printf("Wallclock time elapsed: %.2lf seconds\n",end_time-start_time);
    }


    // Slaves
    if (rank > 0)
    {
        for (n=mystart; n<=LIMIT; n=n+stride)
        {
            if (isprime(n))
            {
                prime_count++;
                recent_prime = n;
            }
        }
        // share the prime number count with the world
        MPI_Reduce(&prime_count,&pcsum,1,MPI_INT,MPI_SUM,0,MPI_COMM_WORLD);
        // Share the max prime
        MPI_Reduce(&recent_prime,&maxprime,1,MPI_INT,MPI_MAX,0,MPI_COMM_WORLD);
    }
    // End MPI process
    MPI_Finalize();
    // Restore governor
    set_governor(szOldGovernor, NULL);
}
