/* This file contains the sequential implementation of prime number generation */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include "../../governor/governor_part1.h"
#define LIMIT    (1024*1024*8)     /* Total numbers to be scanned */

/* Function to check if a number is prime or not */
int isprime(int n)
{
    int i,squareroot;
    if (n>10)
    {
        // Get square rooot
        squareroot = (int) sqrt(n);
        // Check if the number is prime or not
        for (i=3; i<=squareroot; i=i+2)
            // Return 0 if the number is divisible by any number
            if ((n%i)==0)
                return 0;
        return 1;
    }
    else
        return 0;
}


int main(int argc, char *argv[])
{
    char szOldGovernor[32];

    // Set governor for changing frequency
    set_governor("userspace", szOldGovernor);
    set_by_min_freq();
    int n, prime_count, recent_prime;
    double start_time,end_time;
    printf("Starting. Numbers to be scanned= %d\n",LIMIT);

    // Measure starting time
    start_time = omp_get_wtime();
    prime_count=4;

    for (n=11; n<=LIMIT; n=n+2)
    {
        // Call function to check if a number is prime or not
        if (isprime(n))
        {
            // Increament number of primes and store recent prime number
            prime_count++;
            recent_prime = n;
        }
    }
    // Measure end time
    end_time=omp_get_wtime();
	// Restore governor
    set_governor(szOldGovernor, NULL);
    printf("Done. Largest prime is %d Total primes %d\n",recent_prime,prime_count);
    printf("Wallclock time elapsed: %.2lf seconds\n",end_time-start_time);
}


