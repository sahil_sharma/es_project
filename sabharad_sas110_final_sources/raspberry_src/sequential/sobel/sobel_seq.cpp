#include<iostream>
#include<omp.h>
#include<cmath>
#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/highgui/highgui.hpp>
#include "../../governor/governor_part1.h"
using namespace std;
using namespace cv;

int xGradient(Mat image, int x, int y)
{
    return image.at<uchar>(y-1, x-1) +
    2*image.at<uchar>(y, x-1) +
    image.at<uchar>(y+1, x-1) -
    image.at<uchar>(y-1, x+1) -
    2*image.at<uchar>(y, x+1) -
    image.at<uchar>(y+1, x+1);
}
int yGradient(Mat image, int x, int y)
{
    return image.at<uchar>(y-1, x-1) +
    2*image.at<uchar>(y-1, x) +
    image.at<uchar>(y-1, x+1) -
    image.at<uchar>(y+1, x-1) -
    2*image.at<uchar>(y+1, x) -
    image.at<uchar>(y+1, x+1);
}
int main()

{

    Mat src, grey, dst;
    double start, end;
    char szOldGovernor[32];
    set_governor("userspace", szOldGovernor);
    set_by_min_freq();
    int gx, gy, sum;
    src= imread("image2.jpg");
    cvtColor(src,grey,CV_BGR2GRAY);
    dst = grey.clone();
    if( !grey.data )
    { return -1; }
    start = omp_get_wtime();
    //#pragma omp parallel for num_threads(8)
    for(int y = 0; y < grey.rows; y++)
    for(int x = 0; x < grey.cols; x++)
    dst.at<uchar>(y,x) = 0;
    //#pragma omp parallel for private (gx, gy, sum) num_threads(8)
    for(int y = 1; y < grey.rows - 1; y++){
        for(int x = 1; x < grey.cols - 1; x++){
            gx = xGradient(grey, x, y);
            gy = yGradient(grey, x, y);
            sum = abs(gx) + abs(gy);
            sum = sum > 255 ? 255:sum;
            sum = sum < 0 ? 0 : sum;
            dst.at<uchar>(y,x) = sum;

        }
    }
    end = omp_get_wtime();
    namedWindow("sobel");
    imshow("sobel", dst);
    namedWindow("grayscale");
    imshow("grayscale", grey);
    namedWindow("Original");
    imshow("Original", src);
    set_governor(szOldGovernor, NULL);
    cout<<"time is: "<<(end-start)<< " seconds" <<endl;
    waitKey();
    return 0;
}
