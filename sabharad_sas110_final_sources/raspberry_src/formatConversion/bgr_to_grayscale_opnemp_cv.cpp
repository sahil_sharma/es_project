#include<iostream>
#include<omp.h>
#include<cmath>
#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/highgui/highgui.hpp>
using namespace std;
using namespace cv;

int xGradient(Mat image, int x, int y)
{
    return image.at<uchar>(y-1, x-1) +
    2*image.at<uchar>(y, x-1) +
    image.at<uchar>(y+1, x-1) -
    image.at<uchar>(y-1, x+1) -
    2*image.at<uchar>(y, x+1) -
    image.at<uchar>(y+1, x+1);
}
int yGradient(Mat image, int x, int y)
{
    return image.at<uchar>(y-1, x-1) +
    2*image.at<uchar>(y-1, x) +
    image.at<uchar>(y-1, x+1) -
    image.at<uchar>(y+1, x-1) -
    2*image.at<uchar>(y+1, x) -
    image.at<uchar>(y+1, x+1);
}
int main()

{

    Mat src, grey, dst;
    double start, end;
    // Read image
    int gx, gy, sum;
    src= imread("image2.jpg");
    // convert to Grayscale
    //cvtColor(src,grey,CV_BGR2GRAY);
    dst = grey.clone();
    if( !grey.data )
    { return -1; }
    // Measure starting time
    start = omp_get_wtime();
    // strip off the individual channels and store in seperate buffers
    // use the standard equation to calculate the grayscale dest value
    // // Run the block as 128 independent threads, its just an indicator, it really just runs 8 threads
    #pragma omp parallel for num_threads(128)
    for(int y = 0; y < src.rows; y++){
        for(int x = 0; x < src.cols; x++){
            Vec3b intensity = src.at<Vec3b>(y, x);
            float blue = intensity.val[0];
            float green = intensity.val[1];
            float red = intensity.val[2];
            dst.at<uchar>(y,x) = .299f * red + .587f * green + .114f * blue;
        }
    }
    end = omp_get_wtime();
    // measure end time

    // display outputs/inputs
    namedWindow("sobel");
    imshow("sobel", dst);
    namedWindow("grayscale");
    imshow("grayscale", grey);
    namedWindow("Original");
    imshow("Original", src);

    cout<<"time is: "<<(end-start)<< " seconds" <<endl;
    waitKey();
    return 0;
}
