#include<iostream>
#include<omp.h>
#include<stdio.h>
#include<cmath>
#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/highgui/highgui.hpp>
using namespace std;
using namespace cv;

int main()

{

    Mat src, grey, dst;

    src= imread("image2.jpg");
    unsigned char *imageInBytes[3];
    imageInBytes[0] = (unsigned char *) malloc(src.rows*src.cols*sizeof(unsigned char));
    imageInBytes[1] = (unsigned char *) malloc(src.rows*src.cols*sizeof(unsigned char));
    imageInBytes[2] = (unsigned char *) malloc(src.rows*src.cols*sizeof(unsigned char));
    FILE *f = fopen("image1.dat", "wb");


    fwrite((char *)&src.rows, sizeof(src.rows), 1, f);
    fwrite((char *)&src.cols, sizeof(src.rows), 1, f);
    // strip off the individual channels and store in seperate buffers
    for(int y = 0; y < src.rows; y++){
        for(int x = 0; x < src.cols; x++){
            Vec3b intensity = src.at<Vec3b>(y, x);
            imageInBytes[0][y*src.cols + x] = intensity.val[0];
            imageInBytes[1][y*src.cols + x] = intensity.val[1];
            imageInBytes[2][y*src.cols + x] = intensity.val[2];
        }
    }

    //dump the individual channels for our C/MPI code (they take individual buffers as input for convineince )

    // file format : <row><col><image buffer data>

    int r = fwrite ((char *)imageInBytes[0], sizeof(unsigned char), src.rows*src.cols, f);
    int r1 = fwrite ((char *) imageInBytes[1], sizeof(unsigned char), src.rows*src.cols, f);
    int r2 = fwrite ((char *) imageInBytes[2], sizeof(unsigned char), src.rows*src.cols, f);
    printf("%d %d %d %d %d\n", (src.rows), src.cols, r, r1, r2);
    fflush(f);
    fclose(f);
    return 0;
}
