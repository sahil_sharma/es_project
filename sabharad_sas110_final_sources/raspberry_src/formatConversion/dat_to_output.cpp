#include<iostream>
#include<omp.h>
#include<cmath>
#include <stdio.h>
#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/highgui/highgui.hpp>
using namespace std;
using namespace cv;

int main()

{

    Mat src;
    int rows, cols;
    // file format : <row><col><image buffer data>
    // output of MPI/OpemMP/C codes is output.dat
    FILE *f = fopen("output.dat", "rb");
    fread(&rows, sizeof(int), 1, f);
    fread(&cols, sizeof(int), 1, f);
    printf("%d %d\n", rows, cols);
    unsigned char *dst_uchar = (unsigned char *) malloc(rows*cols*sizeof(unsigned char));
    fread( dst_uchar, sizeof(unsigned char), rows*cols, f);
    fclose(f);
    // read the image buffer data from the file and use OpenCV calls to display it
    namedWindow("Original");
    src = Mat(rows, cols, CV_8U, dst_uchar);
    imshow("Original", src);
    namedWindow("copy");
    imshow("copy", src);
    waitKey();
    return 0;
}
