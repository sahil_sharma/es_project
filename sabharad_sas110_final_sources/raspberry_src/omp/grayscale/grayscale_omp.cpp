/* This file contains the OpenMP implementation of RGB to grayscale conversion */

#include<iostream>
#include<omp.h>
#include<cmath>
#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/highgui/highgui.hpp>
#include "../../governor/governor_part1.h"
using namespace std;
using namespace cv;


int main()
{
    // Set governor for changing frequency
    char szOldGovernor[32];
    set_governor("userspace", szOldGovernor);
    set_by_max_freq();
    Mat src, grey, dst;
    double start, end;

    // Read image
    int gx, gy, sum;
    src= imread("image2.jpg");
    cvtColor(src,grey,CV_BGR2GRAY);
    dst = grey.clone();
    if( !grey.data )
    {
        return -1;
    }
    // Measure starting time
    start = omp_get_wtime();


    // Convert image from RGB to Grayscale
    // Run the block as 8 independent threads
    #pragma omp parallel for num_threads(8)
    for(int y = 0; y < src.rows; y++)
    {
        for(int x = 0; x < src.cols; x++)
        {
            Vec3b intensity = src.at<Vec3b>(y, x);
            float blue = intensity.val[0];
            float green = intensity.val[1];
            float red = intensity.val[2];
            dst.at<uchar>(y,x) = .299f * red + .587f * green + .114f * blue;
        }
    }
    // Measure end time
    end = omp_get_wtime();

    // Display Images
    namedWindow("sobel");
    imshow("sobel", dst);
    namedWindow("grayscale");
    imshow("grayscale", grey);
    namedWindow("Original");
    imshow("Original", src);

    cout<<"time is: "<<(end-start)<< " seconds" <<endl;
    waitKey();
    // Restore governor
    set_governor(szOldGovernor, NULL);
    return 0;
}
