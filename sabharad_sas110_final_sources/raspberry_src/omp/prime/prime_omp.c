/* This file contains the OpenMP implementation of prime number generation */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include "../../governor/governor_part1.h"
#define LIMIT    (1024*1024*8)     /* Total numbers to be scanned */

/* Function to check if a number is prime or not */
int isprime(int n)
{
    int i,squareroot;
    if (n>10)
    {
        // Get square rooot
        squareroot = (int) sqrt(n);
        // Check if the number is prime or not
        for (i=3; i<=squareroot; i=i+2)
            // Return 0 if the number is divisible by any number
            if ((n%i)==0)
                return 0;
        return 1;
    }
    else
        return 0;
}


int main(int argc, char *argv[])
{
    int n, prime_count, recent_prime = -1;
    double start_time,end_time;
    // Set governor for changing frequency
    char szOldGovernor[32];
    set_governor("userspace", szOldGovernor);
    set_by_min_freq();
    printf("Starting. Numbers to be scanned= %d\n",LIMIT);

    // Measure starting time
    start_time = omp_get_wtime();
    prime_count=4;

    // Run the function as 8 independent threads
    #pragma omp parallel for num_threads(8) shared (prime_count) reduction(max: recent_prime)
    for (n=11; n<=LIMIT; n=n+2)
    {
        if (isprime(n))
        {
            #pragma omp critical
            // Increament number of primes and store recent prime number
            prime_count++;
            recent_prime = (recent_prime < n) ? n : recent_prime;
        }
    }
    // Measure end time
    end_time=omp_get_wtime();
    printf("Done. Largest prime is %d Total primes %d\n",recent_prime,prime_count);
    printf("Wallclock time elapsed: %.2lf seconds\n",end_time-start_time);
	// Restore governor
    set_governor(szOldGovernor, NULL);
    return 0;
}
