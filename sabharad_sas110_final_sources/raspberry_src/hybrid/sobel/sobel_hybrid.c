#include<omp.h>
#include<unistd.h>
#include<math.h>
#include <stdio.h>
#include "mpi.h"
#include <stdlib.h>

int xGradient(unsigned char * image, int x, int y, int cols)
{
    return image[((y-1)*cols + x-1)] +
    2*image[(y*cols + x-1)] +
    image[(y+1)*cols + x-1] -
    image[(y-1)*cols + x+1] -
    2*image[(y)*cols + x+1] -
    image[(y+1)*cols + x+1];
}
int yGradient(unsigned char * image, int x, int y, int cols)
{
    return image[(y-1)*cols + x-1] +
    2*image[(y-1)*cols + x] +
    image[(y-1)*cols + x+1] -
    image[(y+1)*cols + x-1] -
    2*image[(y+1)*cols + x] -
    image[(y+1)*cols + x+1];
}

int main(int argc, char *argv[])

{
    unsigned char *arr1=NULL,*arr2=NULL,*arr3=NULL;
    int rank,numtasks;
    int rows, cols;
    int count=0;
    int tcount=0;
    unsigned char* edge = NULL;
    unsigned char* grey = NULL;
    unsigned char* out = NULL;
    // Initialize MPI world
    MPI_Init(&argc,&argv);
    // Get communication rank
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    // Get world size
    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
    if(rank==0)
    {   // Read image only in master
        FILE *f = fopen("image1_grey.dat", "rb");
        fread(&rows, sizeof(int), 1, f);
        printf("\nRows: %d",rows);
        fread(&cols, sizeof(int), 1, f);
        printf("\nCols: %d",cols);
        arr1 = (unsigned char *) malloc(rows*cols*sizeof(unsigned char));
        edge = (unsigned char *) malloc(rows*cols*sizeof(unsigned char));
        printf("\n1");
        int  r1 = fread( arr1, sizeof(unsigned char), rows*cols, f);
        printf("\n5");
        fflush(stdout);
        fclose(f);
        printf("Read completed");
        if((rows)%numtasks!=0)
        {
            printf("\nError not a multiple");
            return 1;
        }
        count = (rows*cols)/numtasks;
        tcount = rows*cols;
    }
    // Broadcast the size of images
    MPI_Bcast(&count,1,MPI_INT,0,MPI_COMM_WORLD);
    MPI_Bcast(&cols,1,MPI_INT,0,MPI_COMM_WORLD);
    MPI_Bcast(&tcount,1,MPI_INT,0,MPI_COMM_WORLD);
    printf("\n Count per process : %d ",count);
    // Allocate memory for images locally
    grey=(unsigned char *) malloc (count*sizeof(unsigned char));
    out=(unsigned char *) malloc (count*sizeof(unsigned char));
    // Measure starting time
    double start_time,end_time;
    start_time = MPI_Wtime();   /* Initialize start time */
    // Scatter parts of image to different nodes
    MPI_Scatter(arr1,count,MPI_UNSIGNED_CHAR,grey,count,MPI_UNSIGNED_CHAR,0,MPI_COMM_WORLD);
    int i;
    int x,y,gx,gy,sum=0;
    // run block as 8 independent threads
    // Initialize output to zeros as default output
    #pragma omp parallel for num_threads(8)
    for(y = 0; y < (count/cols); y++)
        for(x = 0; x < cols; x++)
            out[y*cols + x] = 0;
    // run gradient based sobel edge detection on the local image buffer
    // run block as 8 threads, need gx, gy, sum as private to not cause race condition
    #pragma omp parallel for private (gx, gy, sum) num_threads(8)
    for(y = 1; y < (count/cols) - 1; y++)
    {
        for(x = 1; x < cols - 1; x++)
        {
            gx = xGradient(grey, x, y, cols);
            gy = yGradient(grey, x, y, cols);
            sum = abs(gx) + abs(gy);
            sum = sum > 255 ? 255:sum;
            sum = sum < 0 ? 0 : sum;
            out[y*cols + x] = sum;
        }
    }
    // gather the evaluated outputs
    MPI_Gather(out,count,MPI_UNSIGNED_CHAR,edge,count,MPI_UNSIGNED_CHAR,0,MPI_COMM_WORLD);
        // Measure end time
    end_time=MPI_Wtime();
    if(rank==0)
    {   // Write image only in master
        // fix the corner cases iteratively in master;
        // (rows/cluster_size)*cols size of each MPI node, corner cases are the rows that share boundary with other cluster buffers
        // evaluated using the same gradient based algorithm
        for(i=1;i<=numtasks;i++)
        {
            for(y = -1; y <=1; y++)
                for(x = 1; x < cols - 1; x++)
                {
                    if ((y + (count/cols)*i) < (rows - 1)){
                        gx = xGradient(arr1, x, y + (count/cols)*i, cols);
                        gy = yGradient(arr1, x, y + (count/cols)*i, cols);
                        sum = abs(gx) + abs(gy);
                        sum = sum > 255 ? 255:sum;
                        sum = sum < 0 ? 0 : sum;
                        edge[(y +(count/cols)*i) *cols + x] = sum;
                    }
                }
        }
        printf("Edge detection complete. Time %lf", end_time - start_time);
        // Write image only in master
        FILE *f = fopen("output.dat", "wb");
        fwrite(&rows, sizeof(int), 1, f);
        fwrite(&cols, sizeof(int), 1, f);
        fwrite ((unsigned char *)edge, sizeof(unsigned char), rows*cols,f);
        printf("%d %d\n", rows, cols);
        fflush(f);
        fclose(f);
        free(edge);
        free(arr1);
    }
    MPI_Finalize();

    return 0;
}
